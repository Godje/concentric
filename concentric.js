/**
 * Concentric
 * A visual clock
 *
 * Inspired by & based on:
 * https://codepen.io/motorlatitude/pen/uevDx
 */

'use strict'

const {hash} = window.location
let BG = '#030303', FG = '#f8f8f8'

if (hash) {
  const ch = h => /^#[0-9a-f]{3}(?:[0-9a-f]{3})?$/i.test(h)
  const s = hash.split('-')
  const a = s[0]
  const b = `#${s[1]}`
  BG = ch(a) ? a : BG
  FG = ch(b) ? b : FG
}

const x = c.getContext('2d')
const SIZE = 500
const HALF = SIZE / 2
const LW = 3
const SPACE = 29

document.body.style.backgroundColor = BG
c.width = c.height = SIZE
x.strokeStyle = x.fillStyle = FG
x.lineWidth = LW
x.textAlign = 'center'
x.font = '10px monospace'

const months = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('')
const arcV = 3.14 / (2 / 3)

const arcMonth = new Arc(1)
const arcDate = new Arc(2)
const arcBeat = new Arc(3)
const arcPulse = new Arc(4)

function Aequirys (d = new Date) {
  const first = new Date(d.getFullYear(), 0, 1)
  const n = ~~((d - first) / 864E5) + 1
  let date = 0, month = 0

  switch (n) {
    case 365: case 366: break;
    default: {
      date = n - 14 * ~~(n / 14)
      if (date === 0) date = 14
      month = ~~((n - 1) / 14)
      break
    }
  }

  return {month, date}
}

function time () {
  const d = new Date()
  const t = (new Date(d) - d.setHours(0, 0, 0, 0)) / 864E5
  const f = t.toFixed(6).substr(2, 6)
  return [f.substr(0, 3), f.substr(3, 3)]
}

function deg (x, y) {
  return 1.57 * (4 * x - y) / y
}

function Arc (n) {
  this.ROT = 1
  this.RAD = (LW + SPACE) * n

  switch (n) {
    case 1: {
      this.update = function (x) {
        this.ROT = deg(x, 26)
        this.draw()
      }
      break
    }
    case 2: {
      this.update = function (x) {
        this.ROT = deg(x, 14)
        this.draw()
      }
      break
    }
    default: {
      this.update = function (x) {
        this.ROT = deg(x, 1E3)
        this.draw()
      }
      break
    }
  }

  this.draw = function () {
    x.beginPath()
    x.arc(HALF, HALF, this.RAD, arcV, this.ROT, false)
    x.stroke()
  }
}

function display (month, date, beat, pulse) {
  const d = `0${date}`.substr(-2)
  const m = months[month]
  x.fillText(`${d}${m} ${beat}:${pulse}`, HALF, SIZE - 60)
}

function draw (month, date, beat, pulse) {
  x.clearRect(0, 0, SIZE, SIZE)
  arcMonth.update(month)
  arcDate.update(date)
  arcBeat.update(beat)
  arcPulse.update(pulse)
}

(function clock () {
  const {date, month} = Aequirys()
  const [beat, pulse] = time()

  x.save()
  draw(month, date, beat, pulse)
  display(month, date, beat, pulse)
  x.restore()
  window.requestAnimationFrame(clock)
})()
